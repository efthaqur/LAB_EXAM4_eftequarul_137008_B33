<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Modal Example</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="assets/css/footer.css" rel="stylesheet">

</head>

<body>

<?php

    
    $image_name = time().$_FILES['imgFile']['name'];

    $temporary_location = $_FILES['imgFile']['tmp_name'];
    $file_location = 'img/'.$image_name;
    move_uploaded_file($temporary_location, $file_location);
    $fileName = $_POST['fileName'];
    $fileUploadDate = $_POST['date'];


?>

<!-- Begin page content -->
<div class="container">
    <div class="page-header">
        <h1>Example of Carousel</h1>
    </div>

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?php echo $file_location;?>" alt="...">
                <div class="carousel-caption">
                    <?php echo $fileName;?>
                    <br>
                    <?php echo $fileUploadDate;?>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">Eftequarul Alam SEID 137008 BITM Batch 33</p>
    </div>
</footer>


</body>
</html>